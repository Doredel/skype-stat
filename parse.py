import datetime
from parseSpecial import *

_umap = None #username map
_removeChars = r"0123456789,.:;/|\<>[]{}()!?#$%^&*№_+=~«»\"“…—"
_specialSymbols = ["&quot;","&lt;","&gt;","&amp;", "&apos"]

#user map made for people who use multiple accounts, to count them as single person
def setUMap(usermap):
    global _umap

    _umap = usermap

#process single message
def parse(msg):
    global _umap

    author = _umap.get(msg[0],"Other")
    dt = datetime.datetime.fromtimestamp(msg[1])
    y, m, d, h = dt.year, dt.month, dt.day, dt.hour
    words, specials = parseXML(msg[2])
    return ( y, m, d, h ), author, words, specials

#process contents of message XML
def parseXML(xml):
    global _removeChars
    global _specialSymbols

    words = []
    specials = []

    rawxml = xml[:];

    if "<" in xml and "/" in xml and ">" in xml:
        xml,specials = parseSpecial(xml)

    for entry in _specialSymbols: xml = xml.replace(entry, " ") #replace special
    for entry in _removeChars: xml = xml.replace(entry, " ") #replace single characters

    rawWords = [ parseWord(word) for word in xml.split() if word != "" ]

    for word in rawWords:
        if word == "": continue
        valid, spc = checkSpecial(word)
        specials += spc
        if not valid: continue
        words.append(word)

    if len(words) == 0:
        specials.append("#empty")

    if len(words) == 1:
        specials.append("#single")

    specials.append("#message")

    return words, specials

#process single word (entity, separated by space)
def parseWord(word):
    result = word.lower()

    for c in "’`":
        result = result.replace(c, "'")

    result = result.replace("&apos;", "'")
    result = result.replace("–", "-")

    result = result.strip("-' ")

    return result