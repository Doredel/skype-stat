from datetime import datetime

__dict = {}

def setup(dict):
    global __dict

    __dict = dict

def addEntry(year,author,kind,entry):
    dict = getYear(author,year)
    if not entry in dict[kind]:
        dict[kind][entry] = 1
    else:
        dict[kind][entry] += 1

def getYear(author,year):
    global __dict

    if not year in __dict[author]:
        __dict[author][year] = {"words":{},"special":{},"hour":{},"dow":{},"date":{}}

    return __dict[author][year]

def addMsg(fulldate,author):
    weekday = datetime(*fulldate[:3]).weekday()
    sdate = fulldate[0]*10000+fulldate[1]*100+fulldate[2]

    addCount(author, fulldate[0], "hour", fulldate[3])
    addCount(author, fulldate[0], "dow", weekday)
    addCount(author, fulldate[0], "date", sdate)

def addCount(author,year,entity,index):
    global __dict

    counter = getYear(author,year)[entity]
    if not index in counter:
        counter[index] = 1
    else:
        counter[index] += 1

def extract():
    global __dict

    return __dict