import sqlite3

_conn = None #connection

def connect(dbfile):
    global _conn

    _conn = sqlite3.connect(dbfile)

def getUsers(users):
    global _conn

    c = _conn.cursor()
    result = c.execute("select skypename, fullname from contacts where type = 1")

    for user in result:
        if(user[1]=="None"):
            user[1] = user[0]
        if user[0] not in users:
            users.update({user[0]:user[1]})

def getConversations():
    global _conn

    c = _conn.cursor()
    result = c.execute("select id from conversations where type = 2")

    conversations = []

    for conv in result:
        conversations.append(conv[0])

    return conversations

def retrieve(restrict):
    global _conn

    convo_ids = ""

    for num, conv in enumerate(restrict[0]):
        if num > 0: convo_ids += " or "
        convo_ids += "convo_id = " + str(conv)

    convo_ids = "("+convo_ids+")"

    timestamps = "(timestamp >= {} and timestamp < {})".format(*restrict[1:])

    c = _conn.cursor()
    count = c.execute("select count(*) from messages where {} and {}".format(convo_ids,timestamps))
    return count.fetchone()[0], c.execute("select server_id, author, timestamp, body_xml from messages where {} and {}".format(convo_ids,timestamps))

def create():
    global _conn

    c = _conn.cursor()

    c.execute("create table `Special` ( `Author` text not null, `Year` integer not null, `Tag` text not null, `Quan` integer, primary key(`Author`,`Year`,`Tag`) )")
    c.execute("create table `Stats` ( `Author`  text not null, `Year` integer not null, `Stat` text not null, `id` integer not null, `Quan` integer, primary key(`Author`,`Year`,`Stat`,`id`) )")
    c.execute("create table `Words` ( `Author`  text not null, `Year` integer not null, `Word` text not null, `Quan` integer, primary key(`Author`,`Year`,`Word`) )")
    c.execute("create table `Authors` ( `Id`  text not null, `Name` text not null, primary key(`Id`) )")

def wipeAll():
    global _conn

    c = _conn.cursor()
    c.execute("delete from words")
    c.execute("delete from special")
    c.execute("delete from stats")
    c.execute("delete from authors")
    _conn.commit()

def wipeUser(author,year):
    global _conn

    c = _conn.cursor()
    c.execute("delete from words where author = '{}' and year = {}".format(author, year))
    c.execute("delete from special where author = '{}' and year = {}".format(author, year))
    c.execute("delete from stats where author = '{}' and year = {}".format(author, year))
    c.execute("delete from authors where id = '{}'".format(author))
    _conn.commit()

def wipeYear(year):
    global _conn

    c = _conn.cursor()
    c.execute("delete from words where year = {}".format(year))
    c.execute("delete from special where year = {}".format(year))
    c.execute("delete from stats where year = {}".format(year))
    _conn.commit()

def close():
    global _conn

    _conn.close()

def submitAuthors(authors):
    global _conn

    c = _conn.cursor()
    c.executemany("insert into authors values(?,?)", zip(authors.keys(), authors.values()))
    _conn.commit()

def submitWords(author,year,words):
    global _conn

    c = _conn.cursor()
    c.executemany("insert into words values('{}',{},?,?)".format(author,year),words.items())
    _conn.commit()

def submitSpecial(author,year,special):
    global _conn

    c = _conn.cursor()
    c.executemany("insert into special values('{}',{},?,?)".format(author,year),special.items())
    _conn.commit()

def submitStats(author,year,stats):
    global _conn

    c = _conn.cursor()
    c.executemany("insert into stats values('{}',{},'{}',?,?)".format(author, year, "date"), stats["date"].items())
    c.executemany("insert into stats values('{}',{},'{}',?,?)".format(author, year, "dow"), stats["dow"].items())
    c.executemany("insert into stats values('{}',{},'{}',?,?)".format(author, year, "hour"), stats["hour"].items())
    _conn.commit()