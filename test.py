import math
#from dbcon import *
import jsoncon
import parse
import calc
import dbcon
import os.path
from datetime import datetime

dtfrom = datetime(2018, 1, 1)
dtto = datetime(2020, 1, 1)

resourceRoot = r"D:\Projects\Skype Stat" "\\"
resources = [
    #{"db": "20181224.db", "conv_ids":[], "from":0, "to":2483228800}
    {"file":"20200122.json", "conv_ids":{}, "from":dtfrom.timestamp(), "to":dtto.timestamp()}
    ]

parsed_ids = set()

progBlocks = 50

def progress(step,max):
    global progBlocks

    percent = math.ceil(step/max * progBlocks)
    rpercent = math.ceil(step / max * 100)
    prog = "#"*percent + " "*(progBlocks-percent)
    print("\r["+prog+"] "+str(rpercent)+"%", end="")

if __name__ == "__main__":

    users = {}

    for res in resources:
        #file = res['db']
        file = res['file']
        jsoncon.connect(resourceRoot+file)
        print("Retrieving authors from "+file)
        res["conv_ids"] = jsoncon.getConversations()
        users = jsoncon.getUsers(res["conv_ids"])

    parse.setUMap(dict((key, key) for key in users.keys()))
    
    users.update({"other":"Other"})
    calc.setup({user:{} for user in users.keys()})

    iter = 0

    for res in resources:
        file = res['file']
        iter += 1
        jsoncon.connect(resourceRoot+file)
        print("Parsing "+file+":")
        print("["+" "*progBlocks+"] 0%", end = "\r")

        quan, msgs, statuses = jsoncon.retrieve((res["conv_ids"],res["from"],res["to"], users))

        mlimit = quan
        step = 0

        progStep = mlimit//progBlocks

        for id, *msg in msgs:
            step += 1
            if step % progStep == 0: progress(step, mlimit)

            if iter > 1 and id in parsed_ids: continue #message already has been parsed
            parsed_ids.add(id)
            if not msg[2]: continue
            mdata = parse.parse(msg)
            calc.addMsg(mdata[0],mdata[1])
            for word in mdata[2]: calc.addEntry(mdata[0][0],mdata[1],"words",word)
            for special in mdata[3]: calc.addEntry(mdata[0][0], mdata[1], "special", special)
            if step >= mlimit: break
        jsoncon.close()
        progress(mlimit,mlimit)
        print(" Done!")

    print()

    data = calc.extract()

    #save processed info

    if os.path.isfile(resourceRoot+"stats.db"):
        dbcon.connect(resourceRoot+"stats.db")
        dbcon.wipeAll()
    else:
        dbcon.connect(resourceRoot+"stats.db")
        dbcon.create()
    
    dbcon.submitAuthors(users)

    for author, years in data.items():
        for year, stats in years.items():
            print("Submitting data for",author,year, end="")
            dbcon.submitWords(author, year, stats["words"])
            dbcon.submitSpecial(author, year, stats["special"])
            dbcon.submitStats(author, year, stats)
            print(" Done!")

    dbcon.close()
