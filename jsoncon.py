import json
from datetime import datetime
from datetime import tzinfo
from datetime import timedelta

_file = None #connection
_json = None #contents

def connect(jsonfile):
    global _file
    global _json

    _file = open(jsonfile, encoding='utf-8')
    _json = json.load(_file)

#fetch user filter
def getUsers(convs):
    global _json

    users = {}

    for conv in _json["conversations"]:
        if conv["id"] not in convs:
            continue
        for message in conv["MessageList"]:
            if message["from"].endswith("@thread.skype"):
                continue
            
            user = [getUserId(message["from"]), message["displayName"]]
            
            if not user[1]:
                user[1] = user[0]
            if user[0] not in users:
                users.update({user[0]:user[1]})

    return users

#fetch conversation filter
def getConversations():
    global _json
    return [ conv["id"] for conv in _json["conversations"] if conv["threadProperties"] and len(conv["threadProperties"]["members"]) >= 2]

#fetch messages
def retrieve(restrict):
    global _json
    messages = []
    statuses = []
    
    for conv in _json["conversations"]:
        if conv["id"] not in restrict[0]:
            continue
        for message in conv["MessageList"]:
            if getUserId(message["from"]) not in restrict[3]:
                continue

            timestamp = getDateTime(message["originalarrivaltime"])
            if timestamp < restrict[1] or timestamp > restrict[2]:
                continue
            
            authorId = getUserId(message["from"])
            displayName = message["displayName"]
            if displayName and message["content"].startswith(displayName):
                statuses.append((message["id"], authorId, timestamp, message["content"]))
                continue

            messages.append((message["id"], authorId, timestamp, message["content"]))

    return len(messages), messages, statuses

def close():
    global _file

    _file.close()

#discard some weird 8: at the start of user name
def getUserId(id):
    return id[2:]

#normalize date time to local time (for moscow +3)
def getDateTime(string):
    utc = False
   
    if string[-1] == 'Z':
        utc = True
        string = string[:-1]

    i = string.find('.')
    if i != -1:
        string = string[:i]
    
    time = datetime.fromisoformat(string)

    if utc:
        time += timedelta(hours=3)

    return time.timestamp()