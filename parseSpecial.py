#parse skype special xml tags and cut them from message content
def parseSpecial(input):
    specials = []
    input, spc = parseURI(input)
    specials += spc
    input, spc = parseFiles(input)
    specials += spc
    input, spc = parseQuotes(input)
    specials += spc
    input, spc = ParseNumerous(input, "<a href=", "</a>", "#href")
    specials += spc
    input, spc = parseSmiles(input)
    specials += spc
    input, spc = parseMediaAlbum(input)
    specials += spc
    input, spc = parseEdits(input)
    specials += spc
    input, spc = ParseNumerous(input, "<pre raw_pre=", "</pre>", "#dunnowhat")
    specials += spc
    input, spc = ParseNumerous(input, "<at ", "</at>", "#at")
    specials += spc
    input, spc = parseActions(input, ["addmember", "deletemember"], "#adddelete")
    specials += spc
    input, spc = parseActions(input, ["pictureupdate", "topicupdate", "historydisclosedupdate"], "#update")
    specials += spc

    if input.isupper(): specials += ["#shout"]
    return input, specials

def parseTag(input,open,close):
    start = input.find(open)
    if start == -1: return input, False
    end = input.find(close, start)
    if end == -1:
        return input, False
    else:
        end += len(close)
        return input[:start] + input[end:], True

def parseTags(input,open,close):
    total = 0

    input, search = parseTag(input, open, close)

    while search:
        total += 1
        input, search = parseTag(input, open, close)
    return input, total

def parseSmiles(input):
    input, total = parseTags(input,"<ss type=","</ss>")
    return input, ['#smile']*total

def parseQuotes(input):
    input, total = parseTags(input,"<quote author=","</quote>")
    return input, ['#quote']*total

def parseFiles(input):
    *rest, isfile = parseTag(input, "<files alt=", "</files>")
    if isfile: return "", ["#file"]
    return input, []

def parseURI(input):
    *rest, isURI = parseTag(input, "<URIObject ", "</URIObject>")
    if isURI: return "", ["#uri"]
    return input, []

def parseMediaAlbum(input):
    *rest, isAlbum = parseTag(input, "<MediaAlbum ", "</MediaAlbum>")
    if isAlbum: return "", ["#media"]
    return input, []

def parseEdits(input):
    input, ok = parseTag(input, "<e_m ", "</e_m>")
    if ok: 
        return input, ["#edits"]
    return "", []

def parseActions(input, actions, tag):
    for action in actions:
        *rest, ok = parseTag(input, "<" + action + ">", "</" + action + ">")
        if ok: return "", [tag]
    
    return input, []

def ParseNumerous(input, tagOpen, tagClose, tag):
    ok = True
    count = -1

    while ok:
        input, ok = parseTag(input, tagOpen, tagClose)
        count += 1
    
    return input, [tag]*count

def checkSpecial(input):
    specials = []
    valid = True
    if len(input) > 20:
        specials += ["#long"]
        valid = False
    if len(input) < 4:
        specials += ["#short"]
        valid = False
    if len(input) >= 4:
        test = input.replace(input[0],"")
        if len(test) == 0:
            specials += ["#aaaaa"]
            valid = False

    return valid, specials
